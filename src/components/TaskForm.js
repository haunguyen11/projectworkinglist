import React, { Component } from 'react';


class TaskForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            status: true
        }
    }
    componentWillMount() {
        var data = this.props.taskEdit;
        if (data) {
            this.setState({
                id: data.id,
                name: data.name,
                status: data.status
            });
        }
    }

    onChange = (event) => {
        var target = event.target;
        var name = target.name
        var value = target.value;
        this.setState({
            [name]: value
        });
    }
    onSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit(this.state);
    }
    render() {
        return (
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title"> {this.props.taskEdit ? 'Update Task !' : 'Thêm Công Việc'}</h3>
                </div>
                <div class="panel-body">
                    <form onSubmit={this.onSubmit}>
                        <div class="form-group">
                            <label>Tên :</label>
                            <input type="text"
                                class="form-control"
                                name="name"
                                value={this.state.name}
                                onChange={this.onChange}
                            />
                        </div>
                        <label>Trạng Thái :</label>
                        <select class="form-control"
                            name="status"
                            value={this.state.status}
                            onChange={this.onChange}
                            required="required">
                            <option value={true}>Kích Hoạt</option>
                            <option value={false}>Ẩn</option>
                        </select>
                        <br />
                        <div class="text-center">
                            <button type="submit" class="btn btn-warning">Thêm</button>&nbsp;
                        <button type="submit" class="btn btn-danger">Hủy Bỏ</button>
                        </div>
                    </form>
                </div>
            </div>

        );
    }
}
export default TaskForm;
