import React, { Component } from 'react';
import './App.css';
import TaskForm from './components/TaskForm';
import Control from './components/Control';
import TaskList from './components/TaskList';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      isOpenForm: false,
      taskEdit: null,
      filter: {
        filterName: '',
        filterStatus: -1
      }
    }
  }

  OnShowForm = () => {
    this.setState({
      isOpenForm: !this.state.isOpenForm
    })
  }

  OpenForm = () => {
    this.setState({
      isOpenForm: true
    });
  }

  componentWillMount() {
    if (localStorage && localStorage.getItem('tasks')) {
      var tasks = JSON.parse(localStorage.getItem('tasks'));
      this.setState({
        tasks: tasks
      });
    }
  }
  s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

  generateData = () => {
    var tasks = [
      {
        id: this.generateID(),
        name: 'Hoc Lap Trinh',
        status: true
      },
      {
        id: this.generateID(),
        name: 'Hoc Toan',
        status: true
      },
      {
        id: this.generateID(),
        name: 'Di Ngu',
        status: false
      }
      , {
        id: this.generateID(),
        name: 'Hoc Reactjs',
        status: false
      }
    ]
    this.setState({ tasks: tasks });
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }

  generateID = () => {
    return this.s4() + this.s4() + '-' + this.s4() + this.s4();
  }


  onSubmitData = (data) => {
    var tasks = this.state.tasks;
    if (!data.id) {
      var task = {
        id: this.generateID(),
        name: data.name,
        status: data.status
      }
      tasks.push(task);
      this.setState({
        tasks: tasks
      });
      localStorage.setItem('tasks', JSON.stringify(tasks));
    } else {
      tasks.forEach((task, index) => {
        if (task.id === data.id) {
          tasks[index].name = data.name;
          var flag = true;
          if (data.status === "false") {
            flag = false;
          }
          tasks[index].status = flag;

          this.setState({
            tasks: tasks
          });
          localStorage.setItem('tasks', JSON.stringify(tasks));
        }
      });
    }


  }

  onUpdateStatus = (id) => {
    var tasks = this.state.tasks;
    tasks.forEach((task, index) => {
      if (task.id === id) {
        tasks[index].status = !tasks[index].status;
        this.setState({
          tasks: tasks
        });
        localStorage.setItem('tasks', JSON.stringify(tasks));
      }
    });

  }

  onDelete = (id) => {
    var tasks = this.state.tasks;
    tasks.forEach((task, index) => {
      if (task.id === id) {
        tasks.splice(index, 1);
        this.setState({
          tasks: tasks
        });
        localStorage.setItem('tasks', JSON.stringify(tasks));
      }
    });
  }

  onUpdate = (id) => {
    this.OpenForm();

    var tasks = this.state.tasks;

    tasks.forEach((task, index) => {
      if (task.id === id) {
        this.setState({
          taskEdit: task
        });
      }
    });

  }

  onSort = (filterName, filterStatus) => {

    this.setState({
      filter: {
        filterName: filterName,
        filterStatus: parseInt(filterStatus)
      }
    })
  }
  render() {
    var { tasks, isOpenForm, taskEdit, filter } = this.state;
    console.log(filter);
    if (filter) {
      if (filter.filterName) {
        tasks = tasks.filter((task) => {
          return task.name.toLowerCase().indexOf(filter.filterName) !== -1;
        });
      }

      tasks = tasks.filter((task) => {
        if (filter.filterStatus === -1) {
          return task;
        }
        else {
          return task.status === (filter.filterStatus === 1 ? true : false);
        }
      });


    }
    var elmTaskForm = isOpenForm ? <TaskForm onSubmit={this.onSubmitData} taskEdit={taskEdit} /> : "";
    return (
      <div class="container">
        <div class="text-center">
          <h1>Quản Lý Công Việc</h1>
          <hr />
        </div>
        <div class="row">
          <div class={isOpenForm ? "col-xs-4 col-sm-4 col-md-4 col-lg-4" : ""}>
            {elmTaskForm}
          </div>
          <div class={isOpenForm ? "col-xs-8 col-sm-8 col-md-8 col-lg-8" : "col-xs-12 col-sm-12 col-md-12 col-lg-12"}>
            <button type="button"
              class="btn btn-primary mr-5"
              onClick={this.OnShowForm}
            >
              <span class="fa fa-plus mr-5"></span>Thêm Công Việc
                </button>

            <button type="button" class="btn btn-danger"
              onClick={this.generateData}
            >
              <span class="fa fa-plus mr-5"></span>Generate Data
                </button>
            <div class="row mt-15">
              <Control />
            </div>
            <div class="row mt-15">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <TaskList tasks={tasks}
                  onUpdateStatus={this.onUpdateStatus}
                  onUpdate={this.onUpdate}
                  onDelete={this.onDelete}
                  onSort={this.onSort}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}
export default App;
